var page1 = `
<v-list two-line>
    <buetps-list-item title="Hasan Mahmud Prottoy" subtitle="President"></buetps-list-item>
    <buetps-list-item title="Tanveer A Tomal" subtitle="Vice President"></buetps-list-item>
    <buetps-list-item title="Asikul Islam Himel" subtitle="Vice President"></buetps-list-item>
    <buetps-list-item title="Ahmad Asif Sami" subtitle="General Secretary"></buetps-list-item>
    <buetps-list-item title="Imtiar J Niloy" subtitle="Organizing Secretary"></buetps-list-item>
    <buetps-list-item title="Rafid Al Haider" subtitle="Publication Secretary"></buetps-list-item>
    <buetps-list-item title="Naimul Arefin" subtitle="Treasurer and Coordinator"></buetps-list-item>
    <buetps-list-item title="Shoumik Saha" subtitle="Exhibition Secretary"></buetps-list-item>
    <buetps-list-item title="Ahnaf Shahriyar" subtitle="Office Secretary"></buetps-list-item>
    <buetps-list-item title="NH Ridi" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Tanmay Sarkar Akash" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="N R Niloy" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Adib Rahman" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Zulkernain Haider Arnob" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Mostazer Billah" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Anannya Tahsin" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Pushan Alam" subtitle="Executive Member"></buetps-list-item>
</v-list>
`;


var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Batch '13" subtitle="Executive Committee" color="#3a8e77">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;

