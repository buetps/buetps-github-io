var page1 = `
<v-list two-line>
    <buetps-list-item title="Ahmad Asif Sami" subtitle="President"></buetps-list-item>
    <buetps-list-item title="Rafid Al Haider" subtitle="VicePresident"></buetps-list-item>
    <buetps-list-item title="N R Niloy" subtitle="Vice-President"></buetps-list-item>
    <buetps-list-item title="Ahnaf Shahriyar" subtitle="General Secretary"></buetps-list-item>
    <buetps-list-item title="Naimul Arefin" subtitle="Treasurer and Coordinator"></buetps-list-item>
    <buetps-list-item title="Tanmay Sarkar Akash" subtitle="Organizing Secretary"></buetps-list-item>
    <buetps-list-item title="Shoumik Saha" subtitle="Assistant General Secretary - Publication"></buetps-list-item>
    <buetps-list-item title="Adib Rahman" subtitle="Assistant General Secretary - Promotion"></buetps-list-item>
    <buetps-list-item title="Mostazer Billah" subtitle="Logistics Coordinator"></buetps-list-item>
    <buetps-list-item title="Snigdha Shiuly" subtitle="Exhibition Secretary"></buetps-list-item>
    <buetps-list-item title="Pushan Alam" subtitle="Office Secretary"></buetps-list-item>
    
    <buetps-list-item title="Jannatul Adan Meem" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Nafisa Mehtaj" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Abs Rimon" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Zulkernain Haider Arnob" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Anannya Tahsin" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Rahat Chowdhury" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Syed Muhammad" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Yousran Auritra" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Sakib Hasnat" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Mahmud Mahim" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Shakti Banik" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Faizur Himel" subtitle="Executive Member"></buetps-list-item>
</v-list>
`;


var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Batch '14" subtitle="Executive Committee" color="#3a8e77">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;

