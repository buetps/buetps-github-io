var page1 = `
<v-list two-line>
    <buetps-list-item title="Ibrahim Asadullah Tahmid" subtitle="President"></buetps-list-item>
    <buetps-list-item title="Badrul Hasan Tanjil" subtitle="Vice-President"></buetps-list-item>
    <buetps-list-item title="Fahmida Hossain Ela" subtitle="Vice-President"></buetps-list-item>
    <buetps-list-item title="Hasan Mahmud Prottoy" subtitle="General Secretary"></buetps-list-item>
    <buetps-list-item title="Tanveer A Tomal" subtitle="Organizing Secretary"></buetps-list-item>
    <buetps-list-item title="Imtiar J Niloy" subtitle="Exhibition and Publication Secretary"></buetps-list-item>
    <buetps-list-item title="Ahmad Asif Sami" subtitle="Office Secretary"></buetps-list-item>
    <buetps-list-item title="Naimul Arefin" subtitle="Treasurer"></buetps-list-item>
    
    <buetps-list-item title="Jubaid Redwan Rusho" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Fahim Faisal Khan Shapnil" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="NH Ridi" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Tanmay Sarkar Akash" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Rafid Al Haider" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Shoumik Saha" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Adib Rahman" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Ahnaf Shahriyar" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Snigdha Shiuly" subtitle="Executive Member"></buetps-list-item>
</v-list>
`;


var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Batch '12" subtitle="Executive Committee" color="#3a8e77">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;

