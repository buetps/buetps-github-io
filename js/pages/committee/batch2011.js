var page1 = `
<v-list two-line>
    <buetps-list-item title="Dhrubo Jyoti Paul" subtitle="President"></buetps-list-item>
    <buetps-list-item title="Udaya Bir Saha" subtitle="Vice-President"></buetps-list-item>
    <buetps-list-item title="Md. Rizwanul Hasan" subtitle="General Secretary"></buetps-list-item>
    <buetps-list-item title="Mahdi Zaman" subtitle="Organizing Secretary"></buetps-list-item>
    <buetps-list-item title="Badrul Hasan Tanjil" subtitle="Treasurer"></buetps-list-item>
    <buetps-list-item title="Ibrahim Tahmid" subtitle="Promotion and Publication Secretary"></buetps-list-item>
    <buetps-list-item title="Fahmida Hossain Ela" subtitle="Exhibition Secretary"></buetps-list-item>
    <buetps-list-item title="Tanveer Ahmed Tomal" subtitle="Office Secretary"></buetps-list-item>
    <buetps-list-item title="I.H.M. Shamsuzzoha" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Bushra Behrose" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Jubayed Rusho" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Fahim Faisal Khan Shapnil" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Imtiar Jalal Niloy" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Oliur Rahman" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Hasan Mahmud Prottoy" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Ahmed Asif Sami" subtitle="Executive Member"></buetps-list-item>
    <buetps-list-item title="Nasir Hossain" subtitle="Executive Member"></buetps-list-item>
</v-list>
`;


var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Batch '11" subtitle="Executive Committee" color="#3a8e77">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;

