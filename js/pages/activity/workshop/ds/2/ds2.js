var page1 = `
<p>BUET Photographic Society (BUETPS) is delighted to present the second season of “Discovering Streets: An Advanced Street Photography Workshop”. 
        The three day long workshop & review session will be conducted by Asraful Awal Mishuk and Tanvir Murad Topu; Faculty, Pathshala 
        South Asian Media Institute. BUETPS is excited to organize this unique interactive workshop, where the participants will get a 
        hands-on experience unlike any other program. </p>

        <p>This 3 day workshop is segmented in 3 parts.<p>
        <ol>
            <li>Theory Class</li>
            <li>On Street Shooting</li>
            <li>Reviews</li>
        </ol>
        
                
        <h3>Day 1, 27th June</h3>
        <p>09:30am - 12:30pm: Lecture</p>
        <p>A brief History</br>
            What street Photography captures as a genre? Why is it Important?</br>
            Approach and strategies</br>
            References of Masters</p>
        <p>1:30pm onwards - Official Outing</p>
        
        <h3>Day 2, 28th June</h3>
        <p>09:30am - 1:30pm - Review</p>
        <p>3:00pm onwards - Official Outing</p>
        
        <h3>Day 3, 29th June: </h3>
        <p>2:30pm - 6:30pm - Final printed review session and edit</p>
        
        <h4>Who can enter</h4> 
        <p>This workshop is open for all.  Seats are limited. We will be allowing only 30 participants.</p>
        
        <h4>Eligibility for registration</h4> 
        <p>Any photography enthusiasts can apply for this workshop. Participants must have an access to camera (mobile, point & shoot, DSLR).  
        Participants have to be engaged in shooting during the workshop time period. No earlier photographs will be accepted during reviewing. 
        So please apply if you are certain that you will be able to spend all your energy & time in the workshop schedule.</p>
        <p>As this is an advanced street photography workshop, the participants should have basic photography knowledge, skilled at reading 
        exposure quickly and it is encouraged to use lenses of focal length below 24mm (Crop body camera) & 35mm (Full frame camera) for utmost 
        convenience throughout the program. Any format of camera/mobile phones can be used as long as it fulfills aforementioned requirements.</p>
        
        <h4>Registration guidelines</h4>
        <p>Only 30 limited number of seats are available for the workshop. Participants are to fill up a Google doc with their personal information 
        and also submit a portfolio in a PDF format. Based on a preliminary evaluation by the judges, 30 participants will be selected and an Email 
        will be sent to the selected participants. The selected participants then have to pay the registration fee.</p>
        
        <h4>Registration deadline</h4>
        <p>30th May – 20th June 2019</p>
         
        <h4>Registration fee</h4> 
        <p>2000 BDT (Includes Workshop Fee, Food & Snacks, and Prints)</p>
`;


var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Discovering Streets 2" subtitle="An Advanced Street Photography Workshop" color="#3a8e77">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;

