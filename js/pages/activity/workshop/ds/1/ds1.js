var page1 = `
<p>On February 23rd, 2015 BUET Photographic Society announced its newest endeavour, a 3 day long street photography workshop 
which was supposed to be followed by an on campus exhibition. </p>

<p>The workshop is designed to match both the beginners and amateurs who are practicing photography for a while. Street is perhaps 
the most common place where we start to shoot early and Street offers an unbound space to explore . From candid to witty humors, from 
layers of magic to decisive moments it has so much to offer as a single genre that it also demands theoretical dissecting as well as 
rigorous practicing. This workshop is going to cover both the approaches. </p>

<p>From the approach undertaken by BUET Photographic Society, following the leads of the workshop instructor Ashraful Awal Mishuk 
(Faculty, Pathshala South Asian Media Academy), 35 young enthusiast photographers were put to one of the most joyful and yet 
challenging task of a photographer, discovering streets. From the history of the forgotten to the eye soothing examples of the masters, 
these people went through a journey worth being attached to. After that they were challenged on the field to show their skills. They 
spread across the whole Dhaka city and quite interestingly managed to shade a unique perspective to the city we love. After the on 
field shooting sessions our participants received valuable reviews from Ashraful Awal Mishuk, Debasish Shom & Tanvir Murad Topu 
(all three of them are respected faculty members of Pathshala South Asian Media Academy). Our workshop was a grand success and 
after that all the photos taken by the participants went through a rigorous selection and curation phase. So now, here we are 
with the on campus exhibition as promised. This is an exhibition to showcase the honest and youthful output of the workshop participants.</p>


<p>This 3 day workshop is segmented in 3 parts.</p>
<ol>
  <li>Theory Class</li>
  <li>On Street Shooting</li>
  <li>Reviews</li>
</ol>

<p>The Reviews are face to face interaction between the participants and the workshop instructors. Reviewers  will give every participant 
individual time to scrutinize their photographs.</p>

<h3>Day 1 (5th March,2015)</h3>
<p>09:30am - 12:30pm  - Lecture</p>

<p>A brief History</p>

<p>What street Photography captures as a genre? Why is it Important?</p>

<p>Approach and strategies</p>

<p>References of Masters</p>

<p>1:30pm - 5:30pm  - Official Outing</p>

<p>4 hours but participants can also shoot if they want to after the sunset.</p>

<h3>Day 2(6th March,2015)</h3>

<p>09:30am - 1:30pm - Review</p>

<p>3:00pm - 5:30pm  - Official Outing</p>

<p>2.5 hours but participants can also shoot if they want to after the sunset.</p>

<h3>Day 3(7th March,2015)</h3>

<p>Shooting on your own in the morning. Get your Prints done for Final Session</p>

<p>2:30pm - 6:30pm - Final Review and Edit</p>

<h4>Workshop Instructors</h4>

<p>Mishuk Ashraful Awal (Faculty,Pathshala South Asian Media Academy) </br>
Debasish Shom (Faculty, Pathshala South Asian Media Academy)</p>

<h4>Who can enter</h4>

<p>This workshop is open to all.  Seats are limited. We will be allowing only 35 participants.</p>

<h4>Eligibility for Registration</h4>

<p>Any photography enthusiasts both beginner and amateurs can apply for this workshop. Participants must have an access to camera 
( mobile, point & shoot, DSLR ).  Participants have to be engaged in shooting during the workshop time period. 
No earlier photographs will be accepted during reviewing and in exhibition. So please apply if you are  certain that 
you will be able to spend all your energy & time in the workshop schedule.</p>

<h4>How to enter</h4>

<p>You have to submit an online registration form to apply for the workshop & exhibition.</br>
For registration click http://goo.gl/forms/kTIbCS4KV8</p>


<h4>Registration Deadline</h4>

<p>23rd February – 28th February 2015.</p>

<p>Participant list will be announced on 1st March 2015. Participant has to pay the registration fee if he/she gets selected.</p>

<h4>Registration fee</h4>

<p>2000 BDT ( Includes Workshop Fee, Exhibition Fee, Snacks, Prints for both workshop & exhibitions ).</p>

<h4>Workshop Date & Venue</h4>

<p>5th-7th March,Civil Seminar room, Civil Building, BUET.</p>

<h4>The Exhibition</h4>

<p>BUETPS is going to arrange an on campus exhibition with the 35 participant’s Photographs taken during this workshop as well as with 
few selected photographs from BUETPS members. The exhibition is to be held in BUET Campus premise in MARCH.</p>

<h4>Recognition</h4>

<p>Best 3 participants of the workshop will be recognized & each participant will get certificate & prints of their exhibited photos.</p>



<p>Enough with the words, time to enjoy the visual delicacy that is on offer during the exhibition.</p>

<p>Sponsored by ‘CARDIO CARE- A Specialized & General Hospital’. We are thankful to them for their cordial support.</p>
`;

var page2= `
<p>On February 23rd,2015 BUET Photographic Society announced its newest endeavor, a 3 day long street photography workshop which was supposed 
      to be followed by an on campus exhibition.</p>

      <p>From the approach undertaken by BUET Photographic Society, following the leads of the workshop instructor Ashraful Awal Mishuk (Faculty, 
        Pathshala South Asian Media Academy), 35 young enthusiast photographers were put to one of the most joyful and yet challenging task of a 
        photographer, discovering streets. From the history of the forgotten to the eye soothing examples of the masters, these people went through 
        a journey worth being attached to. After that they were challenged on the field to show their skills. They spread across the whole Dhaka city 
        and quite interestingly managed to shade a unique perspective to the city we love. After the on field shooting sessions our participants 
        received valuable reviews from Ashraful Awal Mishuk,Debasish Shom & Tanvir Murad Topu (all three of them are respected faculty members of 
          Pathshala South Asian Media Academy). Our workshop was a grand success and after that all the photos taken by the participants went 
          through a rigorous selection and curation phase. So now, here we are with the on campus exhibition as promised. This is an exhibition 
          to showcase the honest and youthful output of the workshop participants. </p>

      <p>You are cordially invited to enjoy the visual delicacy that will be on offer during the exhibition. Your presence and constructive 
      comments would help the young minds involved to improve.</p>


      <h4>21 March </h4>
      <p>Opening at 1:00PM </p>

      <h4>22-23 March </h4>
      <p>9:00AM-5:00PM </p>

      <h4>Venue</h4>
      <p>Dell Café, BUET </p>


      <p>Our event has been sponsored by ‘CARDIO CARE- A Specialized & General Hospital’. We are thankful to them for their cordial support.</p>
`;

var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Discovering Streets" subtitle="A 3 DAY LONG WORKSHOP & EXHIBITION BY BUETPS" color="#000000">${page1}</general-page-content>
        <general-page-content index=1 title="Discovering Streets" subtitle="A 3 DAY LONG WORKSHOP & EXHIBITION BY BUETPS" color="#000000">${page2}</general-page-content>
    </general-page>
    `
};
  
export default component;

