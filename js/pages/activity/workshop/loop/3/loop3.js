var page1 = `
<p>After a long break, BUETPS is yet again all set and ready to steer up your muscles and wake you up. With its most unique endeavor, 
"Loop of Photography", BUETPS is coming this Wednesday.</p>

<p>Starting from tomorrow, every week on Wednesday, a class will be taken on a particular topic. On Thursday, we are going on an official 
photo-walk. And then, everyone is encouraged to venture on their own, working on that topic, that is, till the next Wednesday, when we will 
arrange a review session with all the participants. </p>

<p>Ideas will be inspired, photos will be reviewed and experiences will be shared. Every participant will be going through a learning phase 
that is worth tens of tutorials. </p>

<p>To begin our exciting session with the loop, we are proud to have our very own Dhrubo Paul (General Secretary, BUETPS) and Mahdi Zaman 
(Exhibition Secretary, BUETPS) to shake things up a little bit who are going to give a talk on the Basics of Photography. We can assure you, 
anyone, who has an enthusiasm about the art of painting with light that we call photography, will find the lesson very handy and effective. 
So, everyone is warmly encouraged to come and join.</p>

<p>We are also delighted to welcome the newest members of the BUETPS family. So, everyone from '14 batch is specially invited as we haven't 
yet got a chance to get acquainted officially. </p>

<p>And of course, the class will be free for all. </p>

<h4>Workshop details</h4>
<h5>Basics of Photography </h5>
<p>Talk given by: Dhrubo Paul and Mahdi Zaman </p>
<p>When: Wednesday, 5 PM, 29 April, 2015 </p>
<p>Where: Room no: 203, Civil Building. </p>

<h5>Portraiture</h5>
<p>Talk given by: Aneek Mustafa Anwar </p>
<p>When: Wednesday, 5 PM, 6 May, 2015 </p>
<p>Where: Room no: 203, Civil Building. </p>

<h5>Review Session</h5>
<p>When: Wednesday, 5 PM, 13 May, 2015 </p>
<p>Where: Room no: 203, Civil Building. </p>
`;


var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Season 3" subtitle="Loop" color="#FF0000">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;
















