var page1 = `
<p>The announcement everyone has been waiting for. The Loop of Photography is back in action! </p>

        <p>Starting March 16, 2016, every Wednesday, we are going to join at a youthful discussion about various aspects on photography. 
        We are going to conduct an official photowalk on the very next day and invite our members to take photos all through the next week. 
        Depending on the performance of our members a number of selected photographs is going to be exhibited on campus. On the next Wednesday the loop will continue.</p>
        
        <p>For our first two session of this season, we give you Shahriar Rashed who will talk about the history of art and Rizwan who will talk on 
        the basic technical aspects of photography.</p>

        <p>In the third and final session Udayan Ghosh will join us and share his views on Composition.</p>
        
        <p>The loop is open for all. Especially for our newest members, it is highly recommended that you join and be a part of the loop.</p>

`;


var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Season 4" subtitle="Loop" color="#F5F5F5">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;
















