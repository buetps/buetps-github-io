var page1 = `
        <p>BUETPS is delighted to announce that the exhibition VoV will take place on October 11-13 at Gallery Drik.</p>

        <p>After rigorous judging and curating phase VoV is all set to exhibit 60 single photographs and 8 photo stories from 56 photographers.</p>
        
        <p>From the Smokey mysterious journey through the life of the Dargah to the vibrant festival of the comic cons, from the classic 
        grammatical photo of a close up face to the objective self-portraits, VoV has broken all the rules and explored the area of 
        Portraiture on such level that no one has ever done before.</p>
        
        <p>The Team VoV is giving their best to make this event a success. We are ready and very excited to see your presence in the exhibition.</p>
        
        <p>Curated by Jashim Salam, “Voyage of Visuals” has been the fruit of his brilliant efforts and dedication. Our sincere gratitude goes towards him.</p>
        
        
        <p>To let you know, VoV is the first exhibition organized by BUETPS that includes participation of photographers from all around Bangladesh.</p>
        
        <p>Exhibition Date: October 11-13, 2014</p>
        <p>Venue: DRIK Gallery, Dhanmondi, Dhaka.</p>
              
        <p>This exhibition is sponsored by Lafarge Surma Cement Limited.</p>
        `;

var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Portraiture" subtitle="Voyage of Visuals" color="#ffea00">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;
