var page1 = `
        <p>BUETPS is delighted to announce that the exhibition for Voyage of Visuals: Belongingness is going to take place from 17th to 19th of May at the Drik Gallery.</p>

        <p>After an eventful month of call for photos and the challenging task of selecting the final set of photos for the exhibition 
        which was conducted by the three honorable judges, Abir Abdullah, M R Hasan and Tanvir Murad Topu, we have 57 single photographs 
        and 9 photo-stories from 53 photographers. </p>
        
        <p>Being a rather challenging theme, our participants have gone to great length to interpret the feel of belongingness in their works. 
        VoV has managed to break the usual rules and explore the brilliance of the Bangladeshi photographers on such levels that no one has ever done before.</p>
        
        <p>Curated by Jashim Salam for the second time in a row, the exhibition of Voyage of Visuals has been the fruit of his brilliant effort and dedication. 
        Our sincere gratitude goes towards him.</p>
        
        <p>Venue: Drik Gallery, Dhanmondi, Dhaka.</p>
        <p>Time: 3pm-8pm</p>
        <p>Date: 17th to 19th May, 2016</p>
        `;

var component = {
    template: `
    <general-page>
        <general-page-content index=0 title="Belongingness" subtitle="Voyage of Visuals" color="#3a8e77">${page1}</general-page-content>
    </general-page>
    `
};
  
export default component;
