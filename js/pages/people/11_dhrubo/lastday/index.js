const lastDay = {
    template: `
        <gallery-item src="/js/pages/people/11_dhrubo/lastday/1.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/lastday/2.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/lastday/3.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/lastday/4.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/lastday/5.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/lastday/6.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/lastday/7.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/lastday/8.jpg"></gallery-item>
    `
};

export default lastDay;