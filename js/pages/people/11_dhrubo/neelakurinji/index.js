const neelakurinji = {
    description : `
        <p>It was a magical cave and I got lost, maybe for a minute
        there, and came out, only to see that fifty years had passed.</p>

        <p>Everything I have done so far, a piece of sheet with a score
        on it, is not relevant anymore. All those dreams which once seemed like
        everything, do not matter anymore. Maybe social media is different now. Where
        do we go to impress people now? Even if it still does exist, do they keep
        unused accounts for that long? I have a few unread messages. But under those
        names, there is written, &quot;Active 30 years ago&quot;. Should I write
        anything, shout in the void like we always do?</p>

        <p>And now, with no one I know around, what am I? When do we
        start being dead anyway? Is it when all the people we have ever known begin to
        believe in our nonexistence? Is that what our restlessness is all about - planting
        a part of our souls in others, accepting the pain that comes with it, just so
        that we may feel like somehow, we matter?</p>

        <p>Perhaps it is the despair of purposelessness, the forced,
        needless misery of living the nonessentials only to stay safe from knowing better.
        You can just set the dial to Default and in the blink of an eye, you can go
        past years. I have. It takes complaining, self-victimization and lots of hard
        work, but it's doable. So, there. </p>

        <p>There is this flower, they call it "Neelakurinji",
        blooms every twelve years, in some mountains of south India. And people from
        faraway places come to see this. Now, would you have ever looked at this crappy
        ass looking flower for a second time if it was not rare? How are we always so
        indifferent to our being and what is it with us about this inclination to the
        things we cannot have? Is it an innate nature of human beings or are we trained
        to compare and envy to save the economy? If we are attracted to only the very
        things we believe we cannot have, where does it leave us? Why do we wish for
        things we believe we cannot get? Is it because it gives us the ability to
        participate in this grandiose competition named &quot;Who can come up with the
        most creative, most 'realistic' excuse?&quot; &quot;I can't. I can't. I
        can't.&quot; The stronger the credibility of the excuse, the higher we get -
        this pure joy of imprisonment, this triumph of indifference.</p>

        <p>In order to get something you want,
        you have to go through only the things you don't want. Because if you don't
        suffer now, why would you get anything in return? It's fairly logical I
        suppose, if logic wasn't the most bendable wood. So, to get to this bliss, you
        have to endure distress, until peace, scares the shit out of you. Until
        thinking about rest tires you with restlessness. Until only stiffness calms you
        down and keeps you on the fence like those video games where you have to keep
        the needle in just the middle green color of a meter, where it is the most
        unstable.</p>

        <p>"You are being too dramatic. I'm sure everything is fine."</p>

        <p>Do more. Be more. Prove more. Crave more. So that everyone
        likes you, except yourself. So that it becomes so blurry that you think it's
        been only a blink of an eye and Cronus keeps you alive just enough so that when
        someone asks, you can lie, saying you are still there.</p>
        `,
    template: `
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/1.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/2.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/3.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/4.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/5.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/6.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/7.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/8.jpg"></gallery-item>
        <gallery-item src="/js/pages/people/11_dhrubo/neelakurinji/9.jpg"></gallery-item>
    `
};

export default neelakurinji;